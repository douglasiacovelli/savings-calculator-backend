class CreateMailUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :mail_users do |t|
      t.string :email
      t.string :nationality

      t.timestamps
    end
  end
end
