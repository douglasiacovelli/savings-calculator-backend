class MailUser < ApplicationRecord
    validates :email, presence: true, uniqueness: true
    validates :nationality, presence: true

    before_save :downcase_params, :strip_params

    private 
    def downcase_params
      self.nationality.downcase!
      self.email.downcase!
    end

    def strip_params
        self.nationality.strip!
        self.email.downcase!
    end
end
