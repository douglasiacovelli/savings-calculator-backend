module Api
  class MailUsersController < ApplicationController
    skip_before_action :verify_authenticity_token

    def create
      @mail_user = MailUser.new(mail_user_params)
      
      if @mail_user.save
        render json: Status::OK, :status => :created
      else
        render json: { 'errors' => @mail_user.errors }, :status => :bad_request
      end

    end

    private
    def mail_user_params
      params.require(:mail_user).permit(:email, :nationality)
    end
  end
end