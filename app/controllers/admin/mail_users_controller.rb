class Admin::MailUsersController < ApplicationController
    before_action :authenticate_admin!

    def index
      if params[:nationality].present?
        @mail_users = MailUser.order(created_at: :desc).where(nationality: params[:nationality])
      else
        @mail_users = MailUser.order(created_at: :desc)
      end
    end
end
