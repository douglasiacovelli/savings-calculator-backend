Rails.application.routes.draw do

  devise_for :admins

  authenticated :admin do
    root 'admin/mail_users#index', as: :authenticated_root
  end

  root "home#login"

  namespace :api do
    resources :mail_users, only: [:create]
  end

  namespace :admin do
    root to: redirect('/admin/mail_users')
    resources :mail_users, only: [:index]
  end

end
